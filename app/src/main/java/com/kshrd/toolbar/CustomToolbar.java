package com.kshrd.toolbar;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class CustomToolbar extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setTitle("កីឡាករ");
        toolbar.setSubtitle("ថ្ងៃអាទិត្យ");

        // Optional
        toolbar.setNavigationIcon(R.drawable.ic_dehaze_black_24dp);

        ToolbarUtil.applyFontForToolbarTitle(this);
        setSupportActionBar(toolbar);
    }


}
